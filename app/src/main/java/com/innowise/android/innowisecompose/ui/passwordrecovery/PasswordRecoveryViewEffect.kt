package com.innowise.android.innowisecompose.ui.passwordrecovery

sealed class PasswordRecoveryViewEffect {
    object Idle : PasswordRecoveryViewEffect()
    object ShowSnackbar : PasswordRecoveryViewEffect()
    object NavigateToEmailMessageScreen : PasswordRecoveryViewEffect()
}