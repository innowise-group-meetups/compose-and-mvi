package com.innowise.android.innowisecompose.ui.music

sealed class MusicGenreViewEffect {
    object Idle : MusicGenreViewEffect()
    object NavigateToMainScreen : MusicGenreViewEffect()
}