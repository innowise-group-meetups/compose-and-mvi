package com.innowise.android.innowisecompose.ui.passwordrecovery

sealed class PasswordRecoveryEvent {
    class ResetLinkButtonClicked(val email: String) : PasswordRecoveryEvent()
}