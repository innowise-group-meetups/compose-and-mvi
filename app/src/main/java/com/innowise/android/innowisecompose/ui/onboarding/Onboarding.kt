package com.innowise.android.innowisecompose.ui.onboarding

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.theme.InnowiseGray
import com.innowise.android.innowisecompose.ui.theme.Shapes
import kotlinx.coroutines.launch

@ExperimentalPagerApi
@Composable
fun Onboarding(onBoardingCompleted: () -> Unit) {
    val context = LocalContext.current

    val listPages = listOf(
        Pair(
            R.drawable.ic_music_folder,
            context.getString(R.string.music_collected_especially_for_you)
        ),
        Pair(
            R.drawable.ic_like,
            context.getString(R.string.sound_that_pleases)
        ),
        Pair(
            R.drawable.ic_earphone,
            context.getString(R.string.listen_on_any_devices_even_without_internet)
        )
    )

    val pagerState = rememberPagerState(
        pageCount = 3,
        initialOffscreenLimit = 2,
        initialPage = 0
    )
    Column {
        val coroutineScope = rememberCoroutineScope()
        Text(
            text = "Skip",
            modifier = Modifier
                .padding(top = 14.dp, end = 24.dp)
                .align(Alignment.End)
                .clickable { onBoardingCompleted() },
            textAlign = TextAlign.End,
            lineHeight = 18.sp,
            fontSize = 14.sp,
            color = Color.White
        )
        Spacer(modifier = Modifier.size(width = 0.dp, 200.dp))
        HorizontalPager(
            state = pagerState,
            modifier = Modifier.fillMaxWidth()
        ) {
            OnboardingPage(listPages[currentPage])
        }
        Spacer(modifier = Modifier.size(48.dp))
        HorizontalPagerIndicator(
            pagerState = pagerState,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            activeColor = Color(0xFFEDF2F4),
            inactiveColor = InnowiseGray
        )
        Box(
            modifier = Modifier
                .padding(bottom = 24.dp)
                .fillMaxSize()
        ) {
            Button(
                onClick = {
                    coroutineScope.launch {
                        if (pagerState.currentPage == pagerState.pageCount - 1) {
                            onBoardingCompleted()
                            return@launch
                        }
                        pagerState.animateScrollToPage(pagerState.currentPage + 1)
                    }
                },
                modifier = Modifier
                    .size(130.dp, 46.dp)
                    .align(Alignment.BottomCenter),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xFFD90429),
                    contentColor = Color.White
                ),
                shape = Shapes.large
            ) {
                Text(text = "Next")
            }
        }
    }
}

@Composable
fun OnboardingPage(pair: Pair<Int, String>) {
    Column(horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.fillMaxWidth()) {
        Image(
            painter = painterResource(id = pair.first),
            contentDescription = null,
            modifier = Modifier.size(100.dp)
        )

        Spacer(modifier = Modifier.size(24.dp))
        Text(
            text = pair.second,
            textAlign = TextAlign.Center,
            color = Color.White,
            fontSize = 24.sp,
            lineHeight = 28.sp,
            fontWeight = FontWeight.W700,
            fontStyle = FontStyle.Normal
        )
    }
}