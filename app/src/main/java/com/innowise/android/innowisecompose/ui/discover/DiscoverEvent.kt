package com.innowise.android.innowisecompose.ui.discover

sealed class DiscoverEvent private constructor(){
    object Idle : DiscoverEvent()
    object FetchData : DiscoverEvent()
}
