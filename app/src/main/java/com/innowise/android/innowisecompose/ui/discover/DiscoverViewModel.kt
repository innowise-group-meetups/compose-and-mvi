package com.innowise.android.innowisecompose.ui.discover

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch

class DiscoverViewModel : ViewModel() {

    private val _songsStateFlow: MutableStateFlow<DiscoverState> =
        MutableStateFlow(DiscoverState())
    val songsStateFlow = _songsStateFlow.asStateFlow()

    fun sendEvent(event: DiscoverEvent) {
        viewModelScope.launch {
            when (event) {
                DiscoverEvent.FetchData -> {
                    _songsStateFlow.emit(songsStateFlow.value.copy(recommendedSongsList = createRecommended()))
                }
                DiscoverEvent.Idle -> {
                }
            }
        }
    }

    private fun createRecommended(): List<Song> =
        listOf(
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
            createSong(),
        )

    private fun createSong() = Song(
        "https://is3-ssl.mzstatic.com/image/thumb/Music124/v4/cf/8b/62/cf8b6217-8b56-b71a-042f-27bd948fb187/20UMGIM76823.rgb.jpg/400x400bb.jpeg",
        "Scream Drive Faster",
        "LAUREL",
        "04:01"
    )
}