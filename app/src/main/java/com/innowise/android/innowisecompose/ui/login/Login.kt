package com.innowise.android.innowisecompose.ui.login

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.theme.InnowiseGray
import com.innowise.android.innowisecompose.ui.theme.Shapes
import com.innowise.android.innowisecompose.ui.theme.Typography

@Composable
fun Login(
    onForgotPassword: () -> Unit,
    onSingInButtonClicked: () -> Unit,
    onSignUpButtonClicked: () -> Unit
) {
    var loginUserText by remember { mutableStateOf("") }
    var passwordText by remember { mutableStateOf("") }

    Column(modifier = Modifier.padding(horizontal = 24.dp)) {
        Spacer(modifier = Modifier.height(36.dp))
        Image(
            painter = painterResource(id = R.drawable.ic_logo_musification),
            contentDescription = null,
            alignment = Alignment.Center,
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(36.dp))
        InnowiseTextField(
            currentText = loginUserText,
            onTextChanged = {loginUserText = it},
            placeholderId = R.string.email_or_username,
            startIconId = R.drawable.ic_profile
        )
        InnowiseTextField(
            modifier = Modifier.padding(top = 24.dp),
            currentText = passwordText,
            onTextChanged = {passwordText = it},
            placeholderId = R.string.password,
            startIconId = R.drawable.ic_lock
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = stringResource(R.string.forgot_your_password),
            modifier = Modifier.clickable { onForgotPassword() },
            style = Typography.caption
        )
        Spacer(modifier = Modifier.weight(1f))
        Button(
            modifier = Modifier
                .height(46.dp)
                .fillMaxWidth(),
            onClick = onSingInButtonClicked,
            shape = Shapes.large,
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFFD90429))
        ) {
            Text(text = "Sign in", color = Color(0xFFEDF2F4))
        }
        Spacer(modifier = Modifier.height(12.dp))
        OrDivider()
        Spacer(modifier = Modifier.height(12.dp))
        Button(
            modifier = Modifier
                .height(46.dp)
                .fillMaxWidth(),
            onClick = { /*TODO*/ },
            shape = Shapes.large,
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFFD90429))
        ) {
            Text(text = "Sign in", color = Color(0xFFEDF2F4))
        }
        Spacer(modifier = Modifier.height(16.dp))
        SignUpRowAction(onSignUpButtonClicked = onSignUpButtonClicked)
        Spacer(modifier = Modifier.height(24.dp))
    }
}

@Composable
fun InnowiseTextField(
    modifier: Modifier = Modifier,
    currentText: String,
    onTextChanged: (String) -> Unit,
    @StringRes placeholderId: Int,
    @DrawableRes startIconId: Int
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .background(Color(0xFF4E556D), Shapes.medium)
            .height(40.dp)
            .padding(horizontal = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            modifier = Modifier.padding(end = 8.dp),
            painter = painterResource(id = startIconId),
            contentDescription = null,
            tint = InnowiseGray
        )
        InputField(currentText, onTextChanged, placeholderId)
    }
}

@Composable
fun InputField(
    currentText: String,
    onTextChanged: (String) -> Unit,
    @StringRes placeholderId: Int
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(18.dp),
        contentAlignment = Alignment.CenterStart
    ) {
        if (currentText.isEmpty()) {
            Text(
                modifier = Modifier.align(Alignment.CenterStart),
                text = stringResource(placeholderId),
                color = InnowiseGray,
                fontSize = 14.sp
            )
        }
        BasicTextField(
            modifier = Modifier.fillMaxWidth(),
            singleLine = true,
            value = currentText,
            onValueChange = { onTextChanged(it) },
            textStyle = TextStyle(InnowiseGray, fontSize = 14.sp, lineHeight = 18.sp),
            cursorBrush = SolidColor(InnowiseGray)
        )
    }
}

@Composable
private fun OrDivider() {
    Row {
        Divider(
            modifier = Modifier
                .weight(1f)
                .padding(top = 8.dp), color = Color.White
        )
        Text(
            text = stringResource(R.string.or),
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .align(Alignment.Top),
            color = Color.White
        )
        Divider(
            modifier = Modifier
                .weight(1f)
                .padding(top = 8.dp), color = Color.White
        )
    }
}

@Composable
private fun SignUpRowAction(onSignUpButtonClicked: () -> Unit) {
    Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        Text(
            text = stringResource(R.string.dont_have_an_account),
            style = Typography.caption,
            color = InnowiseGray
        )
        Text(
            modifier = Modifier
                .padding(start = 8.dp)
                .clickable(onClick = onSignUpButtonClicked),
            text = stringResource(R.string.sign_up),
            style = Typography.caption
        )
    }
}