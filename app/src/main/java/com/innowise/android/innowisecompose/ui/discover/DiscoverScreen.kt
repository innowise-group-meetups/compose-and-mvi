package com.innowise.android.innowisecompose.ui.discover

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import coil.transform.RoundedCornersTransformation
import com.google.accompanist.pager.ExperimentalPagerApi
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.common.ToolBar
import com.innowise.android.innowisecompose.ui.theme.*

@Composable
fun DiscoverScreen(viewModel: DiscoverViewModel = viewModel()) {
    viewModel.sendEvent(DiscoverEvent.FetchData)
    Column {
        var index by rememberSaveable { mutableStateOf(0) }
        val state = viewModel.songsStateFlow.collectAsState()
        ToolBar(stringId = R.string.discover, endDrawableId = R.drawable.ic_search)
        Spacer(modifier = Modifier.height(24.dp))
        InnowiseTab(currentTabIndex = index, onTabClicked = { i -> index = i })
        when (index) {
            0 -> SongsItems(songs = state.value.recommendedSongsList)
            1 -> SongsItems(songs = state.value.popularSongsList)
        }
    }
}


@OptIn(ExperimentalPagerApi::class)
@Composable
fun InnowiseTab(
    currentTabIndex: Int,
    onTabClicked: (Int) -> Unit
) {
    BoxWithConstraints(
        modifier = Modifier
            .height(28.dp)
            .padding(horizontal = 24.dp)
            .fillMaxWidth()
            .background(DarkGray, Shapes.small)
    ) {
        val boxWidth = (((constraints.maxWidth) / 2).dp) / LocalDensity.current.density
        Box(
            modifier = Modifier
                .offset { IntOffset(x = ((boxWidth.roundToPx()) * currentTabIndex), y = 0) }
                .fillMaxHeight()
                .background(InnowiseWhite, Shapes.small)
                .width(boxWidth)
        )

        Row(modifier = Modifier.clip(Shapes.small)) {
            val modifier = Modifier
                .fillMaxSize()
                .weight(1f)
            Box(
                modifier = modifier.clickable { onTabClicked(0) },
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = stringResource(R.string.recommended),
                    color = MainColor,
                    style = Typography.body1
                )
            }
            Box(
                modifier = modifier.clickable { onTabClicked(1) },
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = stringResource(R.string.popular),
                    color = MainColor,
                    style = Typography.body1
                )
            }
        }
    }
}

@Composable
fun SongsItems(songs: List<Song>) {
    LazyColumn(
        contentPadding = PaddingValues(top = 24.dp, start = 24.dp, end = 24.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        items(songs) { item ->
            SongItem(
                link = item.link,
                name = item.name,
                artist = item.artist,
                duration = item.duration
            )
            Spacer(modifier = Modifier.height(8.dp))
            Divider(color = InnowiseGray)
        }
    }
}

@OptIn(ExperimentalCoilApi::class)
@Composable
fun SongItem(
    link: String,
    name: String,
    artist: String,
    duration: String
) {
    Row(
        modifier = Modifier.padding(end = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            modifier = Modifier.size(40.dp),
            painter = rememberImagePainter(data = link, builder = {
                transformations(RoundedCornersTransformation(8f))
            }),
            contentScale = ContentScale.Crop,
            contentDescription = null
        )
        Column(
            modifier = Modifier
                .padding(start = 16.dp)
                .fillMaxHeight()
        ) {
            Text(text = name, style = Typography.body1)
            Text(text = artist, style = Typography.body2, color = InnowiseGray)
        }
        Spacer(modifier = Modifier.weight(1f))
        Text(text = duration, color = InnowiseGray, style = Typography.body2)
        Icon(
            modifier = Modifier.padding(start = 4.dp),
            painter = painterResource(id = R.drawable.ic_more_vertical),
            contentDescription = null,
            tint = InnowiseWhite
        )
    }
}