package com.innowise.android.innowisecompose.ui.emailverification

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.theme.*

@OptIn(ExperimentalCoilApi::class)
@Composable
fun EmailVerificationScreen() {
    Column(modifier = Modifier.fillMaxWidth()) {
        Spacer(modifier = Modifier.weight(1f))
        Image(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .size(100.dp),
            painter = rememberImagePainter(data = R.drawable.im_email),
            contentDescription = null
        )
        Spacer(modifier = Modifier.height(24.dp))
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .fillMaxWidth()
                .padding(horizontal = 24.dp),
            text = stringResource(R.string.verification_link_sent),
            textAlign = TextAlign.Center,
            style = Typography.h1
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 36.dp),
            text = stringResource(R.string.check_your_inbox),
            textAlign = TextAlign.Center,
            style = Typography.body2,
            color = InnowiseGray
        )
        Spacer(modifier = Modifier.weight(1f))
        Text(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .fillMaxWidth(),
            text = stringResource(R.string.didnt_get_your_verification_link_question),
            textAlign = TextAlign.Center,
            style = Typography.caption,
            color = InnowiseGray
        )
        Spacer(modifier = Modifier.height(16.dp))
        OutlinedButton(
            onClick = { /*TODO*/ },
            modifier = Modifier
                .height(46.dp)
                .fillMaxWidth()
                .padding(horizontal = 36.dp),
            shape = Shapes.large,
            border = BorderStroke(1.dp, InnowiseGray),
            contentPadding = PaddingValues(0.dp),
            colors = ButtonDefaults.outlinedButtonColors(backgroundColor = MainColor)
        ) {
            Text(
                modifier = Modifier.align(Alignment.CenterVertically),
                text = stringResource(R.string.resend),
                color = InnowiseWhite
            )
        }
        Spacer(modifier = Modifier.height(24.dp))
    }
}

