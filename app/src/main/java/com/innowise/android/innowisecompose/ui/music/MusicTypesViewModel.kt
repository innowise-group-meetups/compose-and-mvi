package com.innowise.android.innowisecompose.ui.music

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class MusicTypesViewModel : ViewModel() {

    private val mutableStateFlow by lazy { MutableStateFlow(MusicTypeState()) }
    val stateFlow = mutableStateFlow.asStateFlow()

    private val _viewEffectMutableSharedFlow = MutableSharedFlow<MusicGenreViewEffect>()
    val viewEffectSharedFlow: SharedFlow<MusicGenreViewEffect> =
        _viewEffectMutableSharedFlow.asSharedFlow()

    init {
        viewModelScope.launch {
            delay(3000)
            mutableStateFlow.emit(stateFlow.value.copy(isAcceptedAccountVerified = false))
        }
    }

    fun sendEvent(event: MusicGenreEvent) {
        viewModelScope.launch {
            when (event) {
                is MusicGenreEvent.ActivateMusicGenre -> {
                    val newList = mutableStateFlow.value.musicTypesList.toMutableList()
                    newList[event.musicTypeId] = newList[event.musicTypeId].copy(isActive = true)
                    mutableStateFlow.emit(
                        mutableStateFlow.value.copy(
                            musicTypesList = newList,
                            shouldDoneButtonBeVisible = newList.shouldDisplayDoneButton()
                        )
                    )
                }
                is MusicGenreEvent.DeactivateMusicGenre -> {
                    val newList = mutableStateFlow.value.musicTypesList.toMutableList()
                    newList[event.musicTypeId] = newList[event.musicTypeId].copy(isActive = false)
                    mutableStateFlow.emit(
                        mutableStateFlow.value.copy(
                            musicTypesList = newList,
                            shouldDoneButtonBeVisible = newList.shouldDisplayDoneButton()
                        )
                    )
                }
                is MusicGenreEvent.DoneButtonClick -> _viewEffectMutableSharedFlow.emit(
                    MusicGenreViewEffect.NavigateToMainScreen
                )
            }
        }
    }

    private fun List<MusicTypeModel>.shouldDisplayDoneButton(): Boolean =
        this.filter { it.isActive }.size >= 3
}