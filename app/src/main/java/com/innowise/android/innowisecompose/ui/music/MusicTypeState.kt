package com.innowise.android.innowisecompose.ui.music

data class MusicTypeState(
    val isAcceptedAccountVerified: Boolean = true,
    val musicTypesList: List<MusicTypeModel> = createMusicGenreModels(),
    val shouldDoneButtonBeVisible: Boolean = false
)

private fun createMusicGenreModels(): List<MusicTypeModel> = listOf(
    MusicTypeModel(
        0,
        "Punk",
        "https://images.unsplash.com/photo-1516562309708-05f3b2b2c238",
        false
    ),
    MusicTypeModel(
        1,
        "Indie",
        "https://images.unsplash.com/photo-1508261301902-79a2d8e78f71",
        false
    ),
    MusicTypeModel(
        2,
        "Metal",
        "https://images.unsplash.com/photo-1519999482648-25049ddd37b1",
        false
    ),
    MusicTypeModel(3, "Pop", "https://images.unsplash.com/photo-1517602302552-471fe67acf66", false),
    MusicTypeModel(4, "Rap", "https://images.unsplash.com/photo-1547609434-b732edfee020", false),
    MusicTypeModel(
        5,
        "Alternative",
        "https://images.unsplash.com/photo-1513096082106-f68f05c8c21c",
        false
    ),
    MusicTypeModel(
        6,
        "Rock",
        "https://images.unsplash.com/photo-1461887046916-c7426e65460d",
        false
    ),
    MusicTypeModel(
        7,
        "Electric",
        "https://images.unsplash.com/photo-1555940451-2480c214446f",
        false
    ),
    MusicTypeModel(8, "Folk", "https://images.unsplash.com/photo-1544965838-54ef8406f868", false),
    MusicTypeModel(
        9,
        "Chillout",
        "https://images.unsplash.com/photo-1552737894-aae873ee2737",
        false
    ),
    MusicTypeModel(10, "Jazz", "https://images.unsplash.com/photo-1554941829-202a0b2403b8", false),
    MusicTypeModel(
        11,
        "Classical",
        "https://images.unsplash.com/photo-1517523267857-911eef21acae",
        false
    )
)