package com.innowise.android.innowisecompose.ui.music

sealed class MusicGenreEvent {
    class ActivateMusicGenre(val musicTypeId: Int) : MusicGenreEvent()
    class DeactivateMusicGenre(val musicTypeId: Int) : MusicGenreEvent()
    object DoneButtonClick : MusicGenreEvent()
}