package com.innowise.android.innowisecompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import com.google.accompanist.pager.ExperimentalPagerApi
import com.innowise.android.innowisecompose.ui.theme.InnowiseComposeTheme
import com.innowise.android.innowisecompose.ui.theme.MainColor

class MainActivity : ComponentActivity() {
    @ExperimentalFoundationApi
    @ExperimentalPagerApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            InnowiseComposeTheme {
                Surface(color = MainColor, modifier = Modifier.fillMaxSize()) {
                    NavGraph()
                }
            }
        }
    }
}

