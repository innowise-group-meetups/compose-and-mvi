package com.innowise.android.innowisecompose.ui.theme

import androidx.compose.ui.graphics.Color
import androidx.core.graphics.toColorInt

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val MainColor = Color(0xFF1F2135)
val BrightRed = Color(0xFFD90429)
val InnowiseGray = Color(0xFF8D99AE)
val DarkGray = Color(0xFF4E556D)
val InnowiseWhite = Color(0xFFEDF2F4)
val DisabledColor = Color(0xFF616980)
val DullRed = Color(0xFFDB405B)
val Green = Color(0xFF24A174)