package com.innowise.android.innowisecompose.ui.main

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.pager.ExperimentalPagerApi
import com.innowise.android.innowisecompose.ui.albums.AllAlbums
import com.innowise.android.innowisecompose.ui.discover.DiscoverScreen
import com.innowise.android.innowisecompose.ui.profile.ProfileScreen
import com.innowise.android.innowisecompose.ui.screen.InnowiseScreen
import com.innowise.android.innowisecompose.ui.theme.InnowiseGray
import com.innowise.android.innowisecompose.ui.theme.MainColor

@OptIn(ExperimentalFoundationApi::class, ExperimentalPagerApi::class)
@Composable
fun MainScreen(globalNavController: NavController) {
    val tabs = remember { MainTabs.values() }
    val navController = rememberNavController()
    Scaffold(
        bottomBar = { MainScreenBottomBar(navController, tabs) },
        backgroundColor = MainColor
    ) {
        NavHost(navController = navController, startDestination = MainDestinations.DISCOVER_ROUTE) {
            composable(MainDestinations.DISCOVER_ROUTE) {
                DiscoverScreen()
            }
            composable(MainDestinations.ALBUMS_ROUTE) {
                AllAlbums(
                    onViewAllTrendingAlbumsClicked =
                    { globalNavController.navigate(InnowiseScreen.TrendingAlbumsScreen.route) },
                    onViewTopAlbumsClicked =
                    { globalNavController.navigate(InnowiseScreen.TopAlbumsScreen.route) }
                )
            }
            composable(MainDestinations.ARTISTS_ROUTE) {

            }
            composable(MainDestinations.FAVOURITES_ROUTE) {
                Text(text = "Not Implemented")
            }
            composable(MainDestinations.PROFILE_ROUTE) { ProfileScreen() }
        }
    }
}

@Composable
fun MainScreenBottomBar(navController: NavController, tabs: Array<MainTabs>) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route ?: MainTabs.DISCOVER.route
    val routes = remember { MainTabs.values().map { it.route } }
    if (currentRoute in routes) {
        BottomNavigation(backgroundColor = MainColor) {
            tabs.forEach { tab ->
                BottomNavigationItem(
                    icon = { Icon(painterResource(tab.icon), contentDescription = null) },
                    label = {
                        Text(
                            stringResource(tab.title),
                            fontSize = 11.sp,
                            fontWeight = FontWeight.Normal
                        )
                    },
                    selected = currentRoute == tab.route,
                    onClick = {
                        if (tab.route != currentRoute) {
                            navController.navigate(tab.route) {
                                popUpTo(navController.graph.startDestinationId) {
                                    saveState = true
                                }
                                launchSingleTop = true
                                restoreState = true
                            }
                        }
                    },
                    alwaysShowLabel = true,
                    selectedContentColor = Color.Red,
                    unselectedContentColor = InnowiseGray
                )
            }
        }
    }
}