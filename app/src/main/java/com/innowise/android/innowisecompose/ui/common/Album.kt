package com.innowise.android.innowisecompose.ui.common

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.innowise.android.innowisecompose.ui.theme.InnowiseGray
import com.innowise.android.innowisecompose.ui.theme.Shapes
import com.innowise.android.innowisecompose.ui.theme.Typography

@Composable
fun Album(
    imageSize: Dp = Dp.Unspecified,
    columnPaddingValues: PaddingValues = PaddingValues(),
    @DrawableRes imageId: Int
) {
    Column(modifier = Modifier.padding(columnPaddingValues)) {
        Image(
            painter = painterResource(imageId),
            contentDescription = null,
            modifier = Modifier
                .size(imageSize)
                .clip(Shapes.medium),
        )
        Spacer(modifier = Modifier.size(8.dp))
        Text(text = "Cutthroat", style = Typography.body1)
        Spacer(modifier = Modifier.size(2.dp))
        Text(text = "Imagine Dragons", style = Typography.body2, color = InnowiseGray)
    }
}