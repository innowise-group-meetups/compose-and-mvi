package com.innowise.android.innowisecompose.ui.albums

import androidx.annotation.StringRes
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.common.Album
import com.innowise.android.innowisecompose.ui.common.ToolBarWithBackArrowAndEndIcon
import com.innowise.android.innowisecompose.ui.theme.Typography

@ExperimentalFoundationApi
@Composable
fun AllAlbums(
    onViewAllTrendingAlbumsClicked: () -> Unit,
    onViewTopAlbumsClicked: () -> Unit,
) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        ToolBarWithBackArrowAndEndIcon(
            stringId = R.string.albums,
            endDrawableId = R.drawable.ic_search,
            endIconClickListener = {}
        )
        Spacer(modifier = Modifier.size(24.dp))
        AlbumsHeader(
            textId = R.string.now_trending,
            viewAllClickListener = onViewAllTrendingAlbumsClicked
        )
        Spacer(modifier = Modifier.size(16.dp))
        NowTrendingAlbums()
        Spacer(modifier = Modifier.size(24.dp))
        AlbumsHeader(textId = R.string.top_albums, viewAllClickListener = onViewTopAlbumsClicked)
        Spacer(modifier = Modifier.size(16.dp))
        TopAlbumsList()
    }
}

@Composable
private fun NowTrendingAlbums() {
    LazyRow(horizontalArrangement = Arrangement.spacedBy(16.dp)) {
        item { Spacer(modifier = Modifier) }
        items(10) { Album(imageSize = 114.dp, imageId = R.drawable.album_screen) }
        item { Spacer(modifier = Modifier) }
    }
}

@Composable
private fun AlbumsHeader(@StringRes textId: Int, viewAllClickListener: () -> Unit) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 24.dp)
    ) {
        Text(text = stringResource(textId), style = Typography.h2)
        Text(
            text = stringResource(R.string.view_all),
            modifier = Modifier.clickable { viewAllClickListener() },
            style = Typography.caption
        )
    }
}