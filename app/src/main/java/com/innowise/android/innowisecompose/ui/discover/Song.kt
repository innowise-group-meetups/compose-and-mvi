package com.innowise.android.innowisecompose.ui.discover

data class Song(
    val link: String,
    val name: String,
    val artist: String,
    val duration: String
)