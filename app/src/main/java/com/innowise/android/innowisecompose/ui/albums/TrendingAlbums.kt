package com.innowise.android.innowisecompose.ui.albums

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.common.Album
import com.innowise.android.innowisecompose.ui.common.ToolBarWithBackArrowAndEndIcon

@ExperimentalFoundationApi
@Composable
fun TrendingAlbums() {
    Column {
        LocalViewModelStoreOwner
        ToolBarWithBackArrowAndEndIcon(
            stringId = R.string.now_trending,
            endDrawableId = R.drawable.ic_search,
            endIconClickListener = {}
        )
        Spacer(modifier = Modifier.height(24.dp))
        LazyVerticalGrid(
            cells = GridCells.Fixed(2),
            contentPadding = PaddingValues(horizontal = 20.dp)
        ) {
            items(10) {
                Album(columnPaddingValues = PaddingValues(8.dp), imageId = R.drawable.album_layout)
            }
        }
    }
}