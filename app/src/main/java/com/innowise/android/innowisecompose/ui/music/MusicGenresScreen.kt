package com.innowise.android.innowisecompose.ui.music

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.ImageLoader
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import coil.request.CachePolicy
import coil.transform.RoundedCornersTransformation
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.common.BaseInnowiseButton
import com.innowise.android.innowisecompose.ui.theme.*
import androidx.lifecycle.viewmodel.compose.viewModel

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MusicGenresScreen(
    viewModel: MusicTypesViewModel = viewModel(),
    onDoneButtonPressed: () -> Unit
) {
    val state by viewModel.stateFlow.collectAsState()
    val effectState by viewModel.viewEffectSharedFlow.collectAsState(MusicGenreViewEffect.Idle)

    when (effectState) {
        MusicGenreViewEffect.Idle -> {
        }
        MusicGenreViewEffect.NavigateToMainScreen -> onDoneButtonPressed()
    }

    Box {
        Header(state)
        Body(state, viewModel::sendEvent)
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun Body(
    state: MusicTypeState,
    sendEvent: (MusicGenreEvent) -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(60.dp))
        LazyVerticalGrid(
            contentPadding = PaddingValues(horizontal = 16.dp),
            cells = GridCells.Fixed(3)
        ) {
            items(state.musicTypesList) {
                MusicGenre(it) { event -> sendEvent(event) }
            }
        }
        Spacer(
            modifier = Modifier
                .weight(1f)
                .padding(top = 8.dp)
        )
        if (state.shouldDoneButtonBeVisible) {
            BaseInnowiseButton(
                modifier = Modifier
                    .width(130.dp)
                    .padding(bottom = 20.dp),
                clickListener = { sendEvent(MusicGenreEvent.DoneButtonClick) },
                backgroundColor = BrightRed,
                textId = R.string.done
            )
        }
    }
}

@Composable
private fun Header(state: MusicTypeState) {
    if (state.isAcceptedAccountVerified) {
        AccountVerifiedHeader()
    } else {
        GenresTypeHeader()
    }
}

@Composable
private fun GenresTypeHeader() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(44.dp),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(id = R.string.choose_genres_message),
            fontWeight = FontWeight.W600,
            fontSize = 18.sp,
            lineHeight = 20.sp
        )
    }
}

@OptIn(ExperimentalCoilApi::class)
@Composable
private fun MusicGenre(
    model: MusicTypeModel,
    sendEvent: (MusicGenreEvent) -> Unit
) {
    Box(modifier = Modifier.padding(top = 8.dp, start = 8.dp, end = 8.dp)) {
        Column(
            modifier = Modifier.padding(top = 4.dp, end = 4.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val filter = when (model.isActive) {
                true -> ColorMatrix()
                    .apply { setToSaturation(0f) }
                    .run { ColorFilter.colorMatrix(this) }
                false -> null
            }
            Image(
                modifier = Modifier
                    .clip(Shapes.medium)
                    .size(98.dp)
                    .clickable {
                        val event = when (model.isActive) {
                            false -> MusicGenreEvent.ActivateMusicGenre(model.id)
                            true -> MusicGenreEvent.DeactivateMusicGenre(model.id)
                        }

                        sendEvent(event)
                    },
                painter = rememberImagePainter(
                    data = model.iconLink,
                    imageLoader = ImageLoader
                        .Builder(LocalContext.current)
                        .networkCachePolicy(CachePolicy.ENABLED)
                        .memoryCachePolicy(CachePolicy.ENABLED)
                        .build(),
                    builder = { transformations(RoundedCornersTransformation(20f)) }
                ),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                colorFilter = filter
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = model.name, style = Typography.body2)
        }
        if (model.isActive) {
            GenreActiveIndicator(modifier = Modifier.align(Alignment.TopEnd))
        }
    }
}

@Composable
private fun GenreActiveIndicator(modifier: Modifier = Modifier) {
    Box(
        modifier = modifier
            .size(24.dp)
            .background(Green, CircleShape),
        contentAlignment = Alignment.Center
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_circle_check_full),
            tint = MainColor,
            contentDescription = null
        )
    }
}

@Composable
private fun AccountVerifiedHeader() {
    Row(
        modifier = Modifier
            .background(Green)
            .height(60.dp)
            .fillMaxWidth()
            .padding(horizontal = 24.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_success),
            contentDescription = null,
            tint = InnowiseWhite,
        )
        Text(
            modifier = Modifier.padding(start = 16.dp),
            text = stringResource(R.string.account_verified),
            fontSize = 12.sp,
            lineHeight = 14.sp,
            fontWeight = FontWeight.Normal
        )
    }
}
