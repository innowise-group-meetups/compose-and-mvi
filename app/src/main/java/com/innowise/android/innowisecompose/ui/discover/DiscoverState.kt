package com.innowise.android.innowisecompose.ui.discover

data class DiscoverState(
    val recommendedSongsList: List<Song> = emptyList(),
    val popularSongsList: List<Song> = emptyList()
)

