package com.innowise.android.innowisecompose.ui.main

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import com.innowise.android.innowisecompose.R

enum class MainTabs(
    @StringRes val title: Int,
    @DrawableRes val icon: Int,
    val route: String
) {
    DISCOVER(R.string.discover, R.drawable.ic_search, MainDestinations.DISCOVER_ROUTE),
    ALBUMS(R.string.albums, R.drawable.ic_albums, MainDestinations.ALBUMS_ROUTE),
    ARTISTS(R.string.artists, R.drawable.ic_artists, MainDestinations.ARTISTS_ROUTE),
    FAVOURITES(R.string.favourites, R.drawable.ic_heart, MainDestinations.FAVOURITES_ROUTE),
    PROFILE(R.string.profile, R.drawable.ic_profile, MainDestinations.PROFILE_ROUTE)
}

object MainDestinations {
    const val DISCOVER_ROUTE = "main/discover"
    const val ALBUMS_ROUTE = "main/albums"
    const val ARTISTS_ROUTE = "main/artists"
    const val FAVOURITES_ROUTE = "main/favourites"
    const val PROFILE_ROUTE = "main/profile"
}