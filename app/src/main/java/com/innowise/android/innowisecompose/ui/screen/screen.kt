package com.innowise.android.innowisecompose.ui.screen

sealed class InnowiseScreen private constructor(val route: String) {
    object OnboardingScreen : InnowiseScreen("Onboarding")
    object LoginScreen : InnowiseScreen("Login")
    object AllAlbumsScreen : InnowiseScreen("AllAlbums")
    object TrendingAlbumsScreen : InnowiseScreen("TrendingAlbums")
    object TopAlbumsScreen : InnowiseScreen("TopAlbums")
    object PasswordRecoveryScreen : InnowiseScreen("PasswordRecovery")
    object EmailVerificationScreen : InnowiseScreen("EmailVerification")
    object MusicGenresChooseScreen : InnowiseScreen("MusicGenres" )
    object MainScreen : InnowiseScreen("Main")
}