package com.innowise.android.innowisecompose.ui.profile

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.common.ToolBarWithBackArrowAndEndIcon
import com.innowise.android.innowisecompose.ui.theme.InnowiseGray

@OptIn(ExperimentalCoilApi::class)
@Composable
fun ProfileScreen() {
    Column {
        ToolBarWithBackArrowAndEndIcon(
            stringId = R.string.profile,
            endDrawableId = R.drawable.ic_log_out,
            endIconClickListener = {}
        )
        Spacer(modifier = Modifier.size(24.dp))
        ProfileImage(Modifier.align(Alignment.CenterHorizontally))
        Text(
            text = "@username",
            color = Color.White,
            fontSize = 18.sp,
            modifier = Modifier
                .padding(top = 16.dp)
                .align(Alignment.CenterHorizontally)
        )
        Spacer(modifier = Modifier.size(32.dp))
        ProfileItemsRecycler(modifier = Modifier.padding(horizontal = 24.dp))
    }
}

@ExperimentalCoilApi
@Composable
private fun ProfileImage(modifier: Modifier) {
    Box(modifier = modifier) {
        Image(
            painter = rememberImagePainter(
                data = ContextCompat.getDrawable(LocalContext.current, R.drawable.ic_profile_group),
                builder = { crossfade(true) },
            ),
            modifier = Modifier
                .align(Alignment.Center)
                .size(100.dp),
            contentDescription = null
        )
        Box(
            modifier = Modifier
                .size(28.dp)
                .padding(end = 4.dp, bottom = 4.dp)
                .background(Color(0xFFEDF2F4), shape = CircleShape)
                .align(Alignment.BottomEnd),
            contentAlignment = Alignment.Center
        ) {
            Icon(painter = painterResource(id = R.drawable.ic_pen), contentDescription = null)
        }
    }

}

@Composable
fun ProfileItemsRecycler(modifier: Modifier = Modifier) {
    val items = listOf(
        Pair("Personal information", 0),
        Pair("Notifications", 1),
        Pair("Terms of Use", 2),
        Pair("Privacy Policy", 3)
    )
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(12.dp)) {
        items.forEach { item ->
            Divider(color = InnowiseGray)
            ProfileItem(text = item.first)
        }
        Divider(color = InnowiseGray)
    }
}

@Composable
private fun ProfileItem(text: String) {
    val context = LocalContext.current
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = null
            ) {
                Toast
                    .makeText(context, text, Toast.LENGTH_LONG)
                    .show()
            },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = text,
            modifier = Modifier.align(Alignment.CenterVertically),
            color = Color.White
        )
        Icon(
            painter = painterResource(id = R.drawable.ic_arrow_right),
            contentDescription = null,
            tint = Color(0xFFEDF2F4),
            modifier = Modifier.padding(end = 4.dp)
        )
    }
}