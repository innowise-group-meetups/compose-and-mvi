package com.innowise.android.innowisecompose.ui.common

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.innowise.android.innowisecompose.ui.theme.Shapes

@Composable
fun BaseInnowiseButton(
    modifier: Modifier = Modifier,
    clickListener: () -> Unit,
    backgroundColor: Color,
    @StringRes textId: Int
) {
    Button(
        modifier = modifier.height(46.dp),
        onClick = clickListener,
        shape = Shapes.large,
        colors = ButtonDefaults.buttonColors(backgroundColor = backgroundColor)
    ) {
        Text(
            text = stringResource(textId),
            color = Color.White,
            textAlign = TextAlign.Center
        )
    }
}