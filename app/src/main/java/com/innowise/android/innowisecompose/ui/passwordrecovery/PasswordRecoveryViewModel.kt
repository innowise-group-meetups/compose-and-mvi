package com.innowise.android.innowisecompose.ui.passwordrecovery

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class PasswordRecoveryViewModel : ViewModel() {

    private val _viewEffects: MutableSharedFlow<PasswordRecoveryViewEffect> = MutableSharedFlow(0)
    val viewEffects: SharedFlow<PasswordRecoveryViewEffect> = _viewEffects.asSharedFlow()

    fun sendEvent(event: PasswordRecoveryEvent) {
        viewModelScope.launch {
            when (event) {
                is PasswordRecoveryEvent.ResetLinkButtonClicked -> {
                    _viewEffects.emit(PasswordRecoveryViewEffect.ShowSnackbar)
                    delay(5000)
                    _viewEffects.emit(PasswordRecoveryViewEffect.NavigateToEmailMessageScreen)
                }
            }
        }
    }
}