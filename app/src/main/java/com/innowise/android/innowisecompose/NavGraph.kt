package com.innowise.android.innowisecompose

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.pager.ExperimentalPagerApi
import com.innowise.android.innowisecompose.ui.albums.AllAlbums
import com.innowise.android.innowisecompose.ui.albums.TopAlbumsScreen
import com.innowise.android.innowisecompose.ui.albums.TrendingAlbums
import com.innowise.android.innowisecompose.ui.emailverification.EmailVerificationScreen
import com.innowise.android.innowisecompose.ui.login.Login
import com.innowise.android.innowisecompose.ui.main.MainScreen
import com.innowise.android.innowisecompose.ui.music.MusicGenresScreen
import com.innowise.android.innowisecompose.ui.onboarding.Onboarding
import com.innowise.android.innowisecompose.ui.passwordrecovery.PasswordRecoveryScreen
import com.innowise.android.innowisecompose.ui.screen.InnowiseScreen

@OptIn(ExperimentalFoundationApi::class, ExperimentalPagerApi::class)
@Composable
fun NavGraph() {
    val navController = rememberNavController()
    val actions = remember(navController) { MainActions(navController) }

    NavHost(
        navController = navController,
        startDestination = InnowiseScreen.OnboardingScreen.route
    ) {
        composable(InnowiseScreen.OnboardingScreen.route) { Onboarding(actions.onBoardingCompleted) }
        composable(InnowiseScreen.AllAlbumsScreen.route) {
            AllAlbums(
                actions.onViewAllTrendingAlbumsClicked,
                actions.onViewAllTopAlbumsClicked
            )
        }
        composable(InnowiseScreen.TrendingAlbumsScreen.route) { TrendingAlbums() }
        composable(InnowiseScreen.LoginScreen.route) {
            Login(
                actions.onForgotPasswordButtonClicked,
                actions.toMainScreen,
                actions.toMusicGenresScreen
            )
        }
        composable(InnowiseScreen.TopAlbumsScreen.route) { TopAlbumsScreen() }
        composable(InnowiseScreen.PasswordRecoveryScreen.route) {
            PasswordRecoveryScreen(
                onValidationMessageSent = actions.toVerificationMessageScreen
            )
        }
        composable(InnowiseScreen.EmailVerificationScreen.route) { EmailVerificationScreen() }
        composable(InnowiseScreen.MusicGenresChooseScreen.route) {
            MusicGenresScreen(onDoneButtonPressed = actions.toMainScreen)
        }
        composable(InnowiseScreen.MainScreen.route) { MainScreen(navController) }
    }
}

class MainActions(navController: NavController) {

    val onBoardingCompleted: () -> Unit = {
        navController.navigate(InnowiseScreen.LoginScreen.route) { popUpTo(0) }
    }

    val onViewAllTrendingAlbumsClicked: () -> Unit = {
        navController.navigate(InnowiseScreen.TrendingAlbumsScreen.route)
    }

    val onViewAllTopAlbumsClicked: () -> Unit = {
        navController.navigate(InnowiseScreen.TopAlbumsScreen.route)
    }

    val onForgotPasswordButtonClicked: () -> Unit = {
        navController.navigate(InnowiseScreen.PasswordRecoveryScreen.route)
    }

    val toVerificationMessageScreen: () -> Unit = {
        navController.navigate(InnowiseScreen.EmailVerificationScreen.route)
    }

    val toMusicGenresScreen: () -> Unit = {
        navController.navigate(InnowiseScreen.MusicGenresChooseScreen.route)
    }

    val toMainScreen: () -> Unit = { navController.navigate(InnowiseScreen.MainScreen.route) }
}

/**
 * If the lifecycle is not resumed it means this NavBackStackEntry already processed a nav event.
 *
 * This is used to de-duplicate navigation events.
 */
private fun NavBackStackEntry.lifecycleIsResumed() =
    this.lifecycle.currentState == Lifecycle.State.RESUMED