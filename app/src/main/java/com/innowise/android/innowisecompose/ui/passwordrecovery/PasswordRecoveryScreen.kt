package com.innowise.android.innowisecompose.ui.passwordrecovery

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.focusTarget
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.common.BaseInnowiseButton
import com.innowise.android.innowisecompose.ui.common.ToolBarWithBackArrow
import com.innowise.android.innowisecompose.ui.login.InnowiseTextField
import com.innowise.android.innowisecompose.ui.theme.MainColor

@Composable
fun PasswordRecoveryScreen(
    viewModel: PasswordRecoveryViewModel = viewModel(),
    onValidationMessageSent: () -> Unit
) {
    val effectState =
        viewModel
            .viewEffects
            .collectAsState(initial = PasswordRecoveryViewEffect.Idle)
            .value
    var email by remember { mutableStateOf("") }

    Box {
        Column {
            ToolBarWithBackArrow(stringId = R.string.reset_password)
            Spacer(modifier = Modifier.height(24.dp))
            Text(
                modifier = Modifier
                    .padding(horizontal = 24.dp)
                    .fillMaxWidth(),
                text = stringResource(R.string.forgot_your_musification_password),
                color = Color(0xFFEDF2F4),
            )
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                modifier = Modifier
                    .padding(horizontal = 24.dp)
                    .fillMaxWidth(),
                text = stringResource(R.string.enter_your_email_or_username_and_we_will_send_you_a_reminder),
                fontSize = 14.sp,
                textAlign = TextAlign.Justify
            )
            Spacer(modifier = Modifier.height(24.dp))
            InnowiseTextField(
                currentText = email,
                onTextChanged = {email = it},
                placeholderId = R.string.email_or_username,
                startIconId = R.drawable.ic_profile
            )
            Spacer(
                modifier = Modifier
                    .weight(1f)
                    .padding(top = 8.dp)
            )
            BaseInnowiseButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 24.dp),
                clickListener = {
                    viewModel.sendEvent(
                        PasswordRecoveryEvent.ResetLinkButtonClicked(email = email)
                    )
                },
                backgroundColor = Color.Red,
                textId = R.string.send_reset_link
            )
            Spacer(modifier = Modifier.height(24.dp))
        }

        when (effectState) {
            PasswordRecoveryViewEffect.Idle -> {
            }
            PasswordRecoveryViewEffect.ShowSnackbar -> SnackBarOverlay(email)
            PasswordRecoveryViewEffect.NavigateToEmailMessageScreen -> onValidationMessageSent()
        }
    }
}

@Composable
private fun SnackBarOverlay(email: String) {
    Column(modifier = Modifier.focusTarget()) {
        Row(
            modifier = Modifier
                .background(Color.White)
                .height(60.dp)
                .fillMaxWidth()
                .padding(horizontal = 24.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_error),
                contentDescription = null,
                tint = MainColor
            )
            Text(
                text = "Hey $email, Check your inbox, we’ve sent you instructions on how to reset your password!",
                fontWeight = FontWeight.Normal,
                fontSize = 12.sp,
                color = MainColor
            )
        }
        Box(
            modifier = Modifier
                .background(Color(0x66000000))
                .fillMaxSize()
        )
    }
}