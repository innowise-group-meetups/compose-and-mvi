package com.innowise.android.innowisecompose.ui.albums

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.common.Album
import com.innowise.android.innowisecompose.ui.common.ToolBarWithBackArrowAndEndIcon

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TopAlbumsScreen() {
    Column {
        ToolBarWithBackArrowAndEndIcon(
            stringId = R.string.top_albums,
            endDrawableId = R.drawable.ic_search,
            endIconClickListener = {}
        )
        Spacer(modifier = Modifier.size(24.dp))
        TopAlbumsList()
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TopAlbumsList() {
    LazyVerticalGrid(
        cells = GridCells.Fixed(2),
        contentPadding = PaddingValues(horizontal = 16.dp)
    ) {
        items(10) {
            Album(columnPaddingValues = PaddingValues(8.dp), imageId = R.drawable.album_layout)
        }
    }
}