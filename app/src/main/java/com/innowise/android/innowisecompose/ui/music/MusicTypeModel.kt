package com.innowise.android.innowisecompose.ui.music

data class MusicTypeModel(
    val id: Int,
    val name: String,
    val iconLink: String,
    val isActive: Boolean
)