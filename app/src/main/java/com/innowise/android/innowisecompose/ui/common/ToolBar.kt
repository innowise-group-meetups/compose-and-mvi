package com.innowise.android.innowisecompose.ui.common

import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.innowise.android.innowisecompose.R
import com.innowise.android.innowisecompose.ui.theme.InnowiseWhite
import com.innowise.android.innowisecompose.ui.theme.Typography

@Composable
fun ToolBarWithBackArrowAndEndIcon(
    modifier: Modifier = Modifier,
    @StringRes stringId: Int,
    endDrawableId: Int,
    endIconClickListener: (() -> Unit)
) {
    val dispatcher = LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher
    ToolBar(
        modifier = modifier,
        stringId = stringId,
        startDrawableId = R.drawable.ic_arrow_left,
        startIconClickListener = { dispatcher?.onBackPressed() },
        endDrawableId = endDrawableId,
        endIconClickListener = endIconClickListener
    )
}

@Composable
fun ToolBarWithBackArrow(modifier: Modifier = Modifier, @StringRes stringId: Int) {
    val dispatcher = LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher
    ToolBar(
        modifier = modifier,
        stringId = stringId,
        startDrawableId = R.drawable.ic_arrow_left,
        startIconClickListener = { dispatcher?.onBackPressed() }
    )
}

@Composable
fun ToolBar(
    modifier: Modifier = Modifier,
    @StringRes stringId: Int,
    @DrawableRes endDrawableId: Int? = null,
    endIconClickListener: (() -> Unit)? = null,
    @DrawableRes startDrawableId: Int? = null,
    startIconClickListener: (() -> Unit)? = null,
) {
    Box(
        modifier = modifier
            .height(44.dp)
            .fillMaxWidth()
            .padding(horizontal = 24.dp)
    ) {
        if (startDrawableId != null) {
            Icon(
                painter = painterResource(id = startDrawableId),
                contentDescription = null,
                tint = InnowiseWhite,
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .clickable { startIconClickListener?.invoke() }
            )
        }
        Text(
            text = stringResource(id = stringId),
            modifier = Modifier.align(Alignment.Center),
            style = Typography.h3,
            color = InnowiseWhite
        )
        if (endDrawableId != null) {
            Icon(
                painter = painterResource(id = endDrawableId),
                contentDescription = null,
                tint = InnowiseWhite,
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .clickable { endIconClickListener?.invoke() }

            )
        }
    }
}

